<?php

namespace App\Repository;

use App\Amenity;
use App\Repository\AmenityRepositoryInterface;

class AmenityRepository extends BaseRepository implements AmenityRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(Amenity $model)
    {
        $this->model = $model;
    }
}