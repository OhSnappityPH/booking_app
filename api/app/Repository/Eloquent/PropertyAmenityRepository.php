<?php

namespace App\Repository\Eloquent;

use App\PropertyAmenity;
use App\Repository\PropertyAmenityRepositoryInterface;

class PropertyAmenityRepository extends BaseRepository implements PropertyAmenityRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(PropertyAmenity $model)
    {
        $this->model = $model;
    }
}
