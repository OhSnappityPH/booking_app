<?php

namespace App\Repository\Eloquent;

use App\PropertyImage;
use App\Repository\PropertyImageRepositoryInterface;

class PropertyImageRepository extends BaseRepository implements PropertyImageRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(PropertyImage $model)
    {
        $this->model = $model;
    }
}
