<?php

namespace App\Repository\Eloquent;

use App\Subcategory;
use App\Repository\SubcategoryRepositoryInterface;

class SubcategoryRepository extends BaseRepository implements SubcategoryRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(Subcategory $model)
    {
        $this->model = $model;
    }
}
