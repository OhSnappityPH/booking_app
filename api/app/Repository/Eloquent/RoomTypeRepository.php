<?php

namespace App\Repository\Eloquent;

use App\RoomType;
use App\Repository\RoomTypeRepositoryInterface;

class RoomTypeRepository extends BaseRepository implements RoomTypeRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(RoomType $model)
    {
        $this->model = $model;
    }
}
