<?php

namespace App\Repository\Eloquent;

use App\PropertyType;
use App\Repository\PropertyTypeRepositoryInterface;

class PropertyTypeRepository extends BaseRepository implements PropertyTypeRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(PropertyType $model)
    {
        $this->model = $model;
    }
}
