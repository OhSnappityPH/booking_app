<?php

namespace App\Repository\Eloquent;

use App\Booking;
use App\Repository\BookingRepositoryInterface;

class BookingRepository extends BaseRepository implements BookingRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(Booking $model)
    {
        $this->model = $model;
    }
}
