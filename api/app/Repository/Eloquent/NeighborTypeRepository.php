<?php

namespace App\Repository\Eloquent;

use App\NeighborType;
use App\Repository\NeighborTypeRepositoryInterface;

class NeighborTypeRepository extends BaseRepository implements NeighborTypeRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(NeighborType $model)
    {
        $this->model = $model;
    }
}
