<?php

namespace App\Repository\Eloquent;

use App\PromoCode;
use App\Repository\PromoCodeRepositoryInterface;

class PromoCodeRepository extends BaseRepository implements PromoCodeRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(PromoCode $model)
    {
        $this->model = $model;
    }
}
