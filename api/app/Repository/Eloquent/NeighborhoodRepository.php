<?php

namespace App\Repository\Eloquent;

use App\Neighborhood;
use App\Repository\NeighborhoodRepositoryInterface;

class NeighborhoodRepository extends BaseRepository implements NeighborhoodRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(Neighborhood $model)
    {
        $this->model = $model;
    }
}
