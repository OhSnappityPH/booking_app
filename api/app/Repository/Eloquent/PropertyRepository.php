<?php

namespace App\Repository\Eloquent;

use App\Property;
use App\Repository\PropertyRepositoryInterface;

class PropertyRepository extends BaseRepository implements PropertyRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(Property $model)
    {
        $this->model = $model;
    }
}
