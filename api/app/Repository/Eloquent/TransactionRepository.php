<?php

namespace App\Repository\Eloquent;

use App\Transaction;
use App\Repository\TransactionRepositoryInterface;

class TransactionRepository extends BaseRepository implements TransactionRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(Transaction $model)
    {
        $this->model = $model;
    }
}
