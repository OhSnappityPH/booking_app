<?php

namespace App\Repository\Eloquent;

use App\PropertyReview;
use App\Repository\PropertyReviewRepositoryInterface;

class PropertyReviewRepository extends BaseRepository implements PropertyReviewRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor
     * 
     * @param Model $model
     */
    public function __construct(PropertyReview $model)
    {
        $this->model = $model;
    }
}
