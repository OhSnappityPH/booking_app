<?php

namespace App\Providers;

use App\Repository\Eloquent\BaseRepository;
use App\Repository\EloquentRepositoryInterface;
use App\Repository\Eloquent\UserRepository;
use App\Repository\AmenityRepositoryInterface;
use App\Repository\BookingRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\CityRepositoryInterface;
use App\Repository\CountryRepositoryInterface;
use App\Repository\NeighborhoodRepositoryInterface;
use App\Repository\NeighborhoodTypeRepositoryInterface;
use App\Repository\PromoCodeRepositoryInterface;
use App\Repository\PropertyAmenityRepositoryInterface;
use App\Repository\PropertyImageRepositoryInterface;
use App\Repository\PropertyRepositoryInterface;
use App\Repository\PropertyReviewRepositoryInterface;
use App\Repository\PropertyTypeRepositoryInterface;
use App\Repository\RoomTypeRepositoryInterface;
use App\Repository\StateRepositoryInterface;
use App\Repository\SubcategoryRepositoryInterface;
use App\Repository\TransactionRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services
     * 
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(AmenityRepositoryInterface::class, AmenityRepository::class);
        $this->app->bind(BookingRepositoryInterface::class, BookingRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);
        $this->app->bind(CountryRepositoryInterface::class, CountryRepository::class);
        $this->app->bind(NeighborhoodRepositoryInterface::class, NeighborhoodRepository::class);
        $this->app->bind(NeighborhoodTypeRepositoryInterface::class, NeighborhoodTypeRepository::class);
        $this->app->bind(PromoCodeRepositoryInterface::class, PromoCodeRepository::class);
        $this->app->bind(PropertyAmenityRepositoryInterface::class, PropertyAmenityRepository::class);
        $this->app->bind(PropertyImageRepositoryInterface::class, PropertyImageRepository::class);
        $this->app->bind(PropertyRepositoryInterface::class, PropertyRepository::class);
        $this->app->bind(PropertyReviewRepositoryInterface::class, PropertyReviewRepository::class);
        $this->app->bind(PropertyTypeRepositoryInterface::class, PropertyTypeRepository::class);
        $this->app->bind(RoomTypeRepositoryInterface::class, RoomTypeRepository::class);
        $this->app->bind(StateRepositoryInterface::class, StateRepository::class);
        $this->app->bind(SubcategoryRepositoryInterface::class, SubcategoryRepository::class);
        $this->app->bind(TransactionRepositoryInterface::class, TransactionRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     * 
     * @return void
     */
    public function boot()
    {
        //
    }
}