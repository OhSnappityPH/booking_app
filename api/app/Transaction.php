<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function property()
    {
        return $this->belongsTo('App\Property');
    }

    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function promoCode()
    {
        return $this->belongsTo('App\PromoCode');
    }
    
}
