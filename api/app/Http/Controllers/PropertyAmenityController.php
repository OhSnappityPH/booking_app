<?php

namespace App\Http\Controllers;

use App\PropertyAmenity;
use Illuminate\Http\Request;
use App\Http\Requests\PropertyAmenityRequest;
use App\Repository\PropertyAmenityRepositoryInterface;

class PropertyAmenityController extends Controller
{
    private $propertyAmenityRepository;

    public function __construct(PropertyAmenityRepositoryInterface $propertyAmenityRepository)
    {
        $this->propertyAmenityRepository = $propertyAmenityRepository;
    }

    public function index()
    {
        $data = $this->propertyAmenityRepository->all();
        return response()->json($data, 200);
    }

    public function store(PropertyAmenityRequest $request)
    {
        $data = $this->propertyAmenityRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New property amenity created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->propertyAmenityRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(PropertyAmenityRequest $request, $id)
    {
        $data = $this->propertyAmenityRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Property amenity updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->propertyAmenityRepository->deleteById($id);

        return response()->json([
            'message' => 'Property amenity removed.'
        ]);
    }
}
