<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Requests\TransactionRequest;
use App\Repository\TransactionRepositoryInterface;

class TransactionController extends Controller
{
    private $transactionRepository;

    public function __construct(TransactionRepositoryInterface $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function index()
    {
        $data = $this->transactionRepository->all();
        return response()->json($data, 200);
    }

    public function store(TransactionRequest $request)
    {
        $data = $this->transactionRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New transaction created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->transactionRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(TransactionRequest $request, $id)
    {
        $data = $this->transactionRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Transaction updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->transactionRepository->deleteById($id);

        return response()->json([
            'message' => 'Transaction removed.'
        ]);
    }
}
