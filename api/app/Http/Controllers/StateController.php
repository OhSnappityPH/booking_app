<?php

namespace App\Http\Controllers;

use App\State;
use Illuminate\Http\Request;
use App\Http\Requests\StateRequest;
use App\Repository\StateRepositoryInterface;

class StateController extends Controller
{
    private $stateRepository;

    public function __construct(StateRepositoryInterface $stateRepository)
    {
        $this->stateRepository = $stateRepository;
    }

    public function index()
    {
        $data = $this->stateRepository->all();
        return response()->json($data, 200);
    }

    public function store(StateRequest $request)
    {
        $data = $this->stateRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New state added.'
        ]);
    }

    public function show($id)
    {
        $data = $this->stateRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(StateRequest $request, $id)
    {
        $data = $this->stateRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'State updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->stateRepository->deleteById($id);

        return response()->json([
            'message' => 'State removed.'
        ]);
    }
}
