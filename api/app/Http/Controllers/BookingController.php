<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Requests\BookingRequest;
use App\Repository\BookingRepositoryInterface;

class BookingController extends Controller
{
    private $bookingRepository;

    public function __construct(BookingRepositoryInterface $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    public function index()
    {
        $data = $this->bookingRepository->all();
        return response()->json($data, 200);
    }

    public function store(BookingRequest $request)
    {
        $data = $this->bookingRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New booking created.',
        ]);
    }

    public function show($id)
    {
        $data = $this->bookingRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(BookingRequest $request, $id)
    {
        $data = $this->bookingRepository->update($id, $request->validated());
        
        return response()->json([
            'query' => $data,
            'message' => 'Booking updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->bookingRepository->deleteById($id);

        return response()->json([
            'message' => 'Booking removed.',
        ]);
    }
}
