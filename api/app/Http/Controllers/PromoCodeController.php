<?php

namespace App\Http\Controllers;

use App\PromoCode;
use Illuminate\Http\Request;
use App\Http\Requests\PromoCodeRequest;
use App\Repository\PromoCodeRepositoryInterface;

class PromoCodeController extends Controller
{
    private $promoCodeRepository;

    public function __construct(PromoCodeRepositoryInterface $promoCodeRepository)
    {
        $this->promoCodeRepository = $promoCodeRepository;
    }

    public function index()
    {
        $data = $this->promoCodeRepository->all();
        return response()->json($data, 200);
    }

    public function store(PromoCodeRequest $request)
    {
        $data = $this->promoCodeRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New promo code created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->promoCodeRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(PromoCodeRequest $request, $id)
    {
        $data = $this->promoCodeRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Promo code updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->promoCodeRepository->deleteById($id);

        return response()->json([
            'message' => 'Promo code removed.'
        ]);
    }
}
