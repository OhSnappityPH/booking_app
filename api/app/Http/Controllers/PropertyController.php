<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use App\Http\Requests\PropertyRequest;
use App\Repository\PropertyRepositoryInterface;

class PropertyController extends Controller
{
    private $propertyRepository;

    public function __construct(PropertyRepositoryInterface $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    public function index()
    {
        $data = $this->propertyRepository->all();
        return response()->json($data, 200);
    }

    public function store(PropertyRequest $request)
    {
        $data = $this->propertyRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New property created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->propertyRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(PropertyRequest $request, $id)
    {
        $data = $this->propertyRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Property updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->propertyRepository->deleteById($id);

        return response()->json([
            'message' => 'Property removed.'
        ]);
    }
}
