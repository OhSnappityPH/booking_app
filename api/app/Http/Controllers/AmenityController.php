<?php

namespace App\Http\Controllers;

use App\Amenity;
use Illuminate\Http\Request;
use App\Http\Requests\AmenityRequest;
use App\Repository\AmenityRepositoryInterface;

class AmenityController extends Controller
{
    private $amenityRepository;

    public function __construct(AmenityRepositoryInterface $amenityRepository)
    {
        $this->amenityRepository = $amenityRepository;
    }
    
    public function index()
    {
        $data = $this->amenityRepository->all();
        return response()->json($data, 200);
    }

    public function store(AmenityRequest $request)
    {
        $data = $this->amenityRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Amenity added.'
        ]);
    }

    public function show($id)
    {
        $data = $this->amenityRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(AmenityRequest $request, $id)
    {
        $data = $this->amenityRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Amenity updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->amenityRepository->deleteById($id);

        return response()->json([
            'message' => 'Amenity removed',
        ]);
    }
}
