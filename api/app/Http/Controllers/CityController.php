<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use App\Http\Requests\CityRequest;
use App\Repository\CityRepositoryInterface;

class CityController extends Controller
{
    private $cityRepository;

    public function __construct(CityRepositoryInterface $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function index()
    {
        $data = $this->cityRepository->all();
        return response()->json($data, 200);
    }

    public function store(CityRequest $request)
    {
        $data = $this->cityRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New city created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->cityRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $data = $this->cityRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'City updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->cityRepository->deleteById($id);

        return response()->json([
            'message' => 'City removed.'
        ]);
    }
}
