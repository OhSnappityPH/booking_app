<?php

namespace App\Http\Controllers;

use App\PropertyImage;
use Illuminate\Http\Request;
use App\Http\Requests\PropertyImageRequest;
use App\Repository\PropertyImageRepositoryInterface;

class PropertyImageController extends Controller
{
    private $propertyImageRepository;

    public function __construct(PropertyImageRepositoryInterface $propertyImageRepository)
    {
        $this->propertyImageRepository = $propertyImageRepository;
    }

    public function index()
    {
        $data = $this->propertyImageRepository->all();
        return response()->json($data, 200);
    }

    public function store(PropertyImageRequest $request)
    {
        $data = $this->propertyImageRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New propertyImage created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->propertyImageRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(PropertyImageRequest $request, $id)
    {
        $data = $this->propertyImageRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Property Image updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->propertyImageRepository->deleteById($id);

        return response()->json([
            'message' => 'Property Image removed.'
        ]);
    }
}
