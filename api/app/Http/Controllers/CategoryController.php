<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Repository\CategoryRepositoryInterface;

class CategoryController extends Controller
{
    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function index()
    {
        $data = $this->categoryRepository->all();
        return response()->json($data, 200);
    }

    public function store(CategoryRequest $request)
    {
        $data = $this->categoryRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New category created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->categoryRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(CategoryRequest $request, $id)
    {
        $data = $this->categoryRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Category updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->categoryRepository->deleteById($id);

        return response()->json([
            'message' => 'Category removed.'
        ]);
    }
}
