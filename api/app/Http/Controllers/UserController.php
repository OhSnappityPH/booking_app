<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Repository\UserRepositoryInterface;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $data = $this->userRepository->all();
        return response()->json($data, 200);
    }

    public function store(UserRequest $request)
    {
        $data = $this->userRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New user created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->userRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(UserRequest $request, $id)
    {
        $data = $this->userRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'User updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->userRepository->deleteById($id);

        return response()->json([
            'message' => 'User removed.'
        ]);
    }
}
