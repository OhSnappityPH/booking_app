<?php

namespace App\Http\Controllers;

use App\PropertyReview;
use Illuminate\Http\Request;
use App\Http\Requests\PropertyReviewRequest;
use App\Repository\PropertyReviewRepositoryInterface;

class PropertyReviewController extends Controller
{
    private $propertyReviewRepository;

    public function __construct(PropertyReviewRepositoryInterface $propertyReviewRepository)
    {
        $this->propertyReviewRepository = $propertyReviewRepository;
    }
    
    public function index()
    {
        $data = $this->propertyReviewRepository->all();
        return response()->json($data, 200);
    }

    public function store(PropertyReviewRequest $request)
    {
        $data = $this->propertyReviewRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New property review created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->propertyReviewRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(PropertyReviewRequest $request, $id)
    {
        $data = $this->propertyReviewRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Property review updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->propertyReviewRepository->deleteById($id);

        return response()->json([
            'message' => 'Property review removed.'
        ]);
    }
}
