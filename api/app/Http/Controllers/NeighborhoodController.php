<?php

namespace App\Http\Controllers;

use App\Neighborhood;
use Illuminate\Http\Request;
use App\Http\Requests\NeighborhoodRequest;
use App\Repository\NeighborhoodRepositoryInterface;

class NeighborhoodController extends Controller
{
    private $neighborhoodRepository;

    public function __construct(NeighborhoodRepositoryInterface $neighborhoodRepository)
    {
        $this->neighborhoodRepository = $neighborhoodRepository;
    }

    public function index()
    {
        $data = $this->neighborhoodRepository->all();
        return response()->json($data, 200);
    }

    public function store(NeighborhoodRequest $request)
    {
        $data = $this->neighborhoodRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New neighborhood created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->neighborhoodRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(NeighborhoodRequest $request, $id)
    {
        $data = $this->neighborhoodRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Neighborhood updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->neighborhoodRepository->deleteById($id);

        return response()->json([
            'message' => 'Neighborhood removed.'
        ]);
    }
}
