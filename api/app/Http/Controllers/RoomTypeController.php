<?php

namespace App\Http\Controllers;

use App\RoomType;
use Illuminate\Http\Request;
use App\Http\Requests\RoomTypeRequest;
use App\Repository\RoomTypeRepositoryInterface;

class RoomTypeController extends Controller
{
    private $roomTypeRepository;

    public function __construct(RoomTypeRepositoryInterface $roomTypeRepository)
    {
        $this->roomTypeRepository = $roomTypeRepository;
    }

    public function index()
    {
        $data = $this->roomTypeRepository->all();
        return response()->json($data, 200);
    }

    public function store(RoomTypeRequest $request)
    {
        $data = $this->roomTypeRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New room type created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->roomTypeRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(RoomTypeRequest $request, $id)
    {
        $data = $this->roomTypeRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Room type updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->roomTypeRepository->deleteById($id);

        return response()->json([
            'message' => 'Room type removed.'
        ]);
    }
}
