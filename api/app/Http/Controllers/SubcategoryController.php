<?php

namespace App\Http\Controllers;

use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Requests\SubcategoryRequest;
use App\Repository\SubcategoryRepositoryInterface;

class SubcategoryController extends Controller
{
    private $subcategoryRepository;

    public function __construct(SubcategoryRepositoryInterface $subcategoryRepository)
    {
        $this->subcategoryRepository = $subcategoryRepository;
    }

    public function index()
    {
        $data = $this->subcategoryRepository->all();
        return response()->json($data, 200);
    }

    public function store(SubcategoryRequest $request)
    {
        $data = $this->subcategoryRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New subcategory created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->subcategoryRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(SubcategoryRequest $request, $id)
    {
        $data = $this->subcategoryRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Subcategory updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->subcategoryRepository->deleteById($id);

        return response()->json([
            'message' => 'Subcategory removed.'
        ]);
    }
}
