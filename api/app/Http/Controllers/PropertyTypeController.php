<?php

namespace App\Http\Controllers;

use App\PropertyType;
use Illuminate\Http\Request;
use App\Http\Requests\PropertyTypeRequest;
use App\Repository\PropertyTypeRepositoryInterface;

class PropertyTypeController extends Controller
{
    private $propertyTypeRepository;

    public function __construct(PropertyTypeRepositoryInterface $propertyTypeRepository)
    {
        $this->propertyTypeRepository = $propertyTypeRepository;
    }

    public function index()
    {
        $data = $this->propertyTypeRepository->all();
        return response()->json($data, 200);
    }

    public function store(PropertyTypeRequest $request)
    {
        $data = $this->propertyTypeRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New propertyType created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->propertyTypeRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(PropertyTypeRequest $request, $id)
    {
        $data = $this->propertyTypeRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Property Type updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->propertyTypeRepository->deleteById($id);

        return response()->json([
            'message' => 'Property Type removed.'
        ]);
    }
}
