<?php

namespace App\Http\Controllers;

use App\NeighborType;
use Illuminate\Http\Request;
use App\Http\Request\NeighborTypeRequest;
use App\Repository\NeighborTypeRepositoryInterface;

class NeighborTypeController extends Controller
{
    private $neighborTypeRepository;

    public function __construct(NeighborTypeRepositoryInterface $neighborTypeRepository)
    {
        $this->neighborTypeRepository = $neighborTypeRepository;
    }

    public function index()
    {
        $data = $this->neighborTypeRepository->all();
        return response()->json($data, 200);
    }

    public function store(NeighborTypeRequest $request)
    {
        $data = $this->neighborTypeRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New neighborType created.'
        ]);
    }

    public function show($id)
    {
        $data = $this->neighborTypeRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(NeighborTypeRequest $request, $id)
    {
        $data = $this->neighborTypeRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'NeighborType updated.',
        ]);
    }

    public function destroy($id)
    {
        $this->neighborTypeRepository->deleteById($id);

        return response()->json([
            'message' => 'NeighborType removed.'
        ]);
    }
}
