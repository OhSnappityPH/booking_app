<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Requests\CountryRequest;
use App\Repository\CountryRepositoryInterface;

class CountryController extends Controller
{
    private $countryRepository;

    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function index()
    {
        $data = $this->countryRepository->all();
        return response()->json($data, 200);
    }

    public function store(CountryRequest $request)
    {
        $data = $this->countryRepository->create($request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'New country added.'
        ]);
    }

    public function show($id)
    {
        $data = $this->countryRepository->findById($id);
        return response()->json($data, 200);
    }

    public function update(CountryRequest $request, $id)
    {
        $data = $this->countryRepository->update($id, $request->validated());

        return response()->json([
            'query' => $data,
            'message' => 'Country updated.',
        ]);
    }

    public function destroy(Country $country)
    {
        $this->countryRepository->deleteById($id);

        return response()->json([
            'message' => 'Country removed.'
        ]);
    }
}
