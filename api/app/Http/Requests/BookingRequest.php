<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'check_in_date' => 'required|date',
            'check_out_date' => 'required|date',
            'price_per_day' => 'required|numeric',
            'price_per_stay' => 'required|numeric',
            'tax_paid' => 'required|numeric',
            'site_fees' => 'required|numeric',
            'amount_paid' => 'required|numeric',
            'is_refundable' => 'required|boolean', //Accepted input are true, false, 1, 0, "1", and "0".
            'effective_amount' => 'required|numeric',
            'booking_date' => 'required|date',
        ];
    }
}
