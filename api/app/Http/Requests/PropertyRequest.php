<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required|max:255',
            'address' => 'required|max:255',
            'latitude' => 'required',
            'latitude' => 'required',
            'bedroom_count' => 'required',
            'bed_count' => 'required',
            'accommodates_count' => 'required',
            'availability_type' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'price' => 'required|numeric',
            'price_type' => 'required|numeric',
            'minimum_stay' => 'required',
            'minimum_stay_type' => 'required|numeric',
            'refund_type' => 'required|numeric',
        ];
    }
}
