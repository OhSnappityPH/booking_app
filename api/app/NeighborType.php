<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NeighborType extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function neighborhood()
    {
        return $this->hasOne('App\Neighborhood');
    }
}
