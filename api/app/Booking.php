<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function property()
    {
        return $this->belongsTo('App\Property');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
    
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
}
