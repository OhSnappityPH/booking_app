<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Neighborhood extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function neighborhoodType()
    {
        return $this->belongsTo('App\Neighbor');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
