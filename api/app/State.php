<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function properties()
    {
        return $this->hasMany('App\Property');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
    
    public function cities()
    {
        return $this->hasMany('App\City');
    }
}
