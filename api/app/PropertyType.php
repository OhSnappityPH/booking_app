<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyType extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function properties()
    {
        return $this->hasMany('App\Properties');
    }
}
