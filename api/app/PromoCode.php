<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCode extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function transactions()
    {
        return $this->hasOne('App\Transaction');
    }
}
