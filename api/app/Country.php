<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function properties()
    {
        return $this->hasMany('App\Property');
    }

    public function states()
    {
        return $this->hasMany('App\State');
    }
}
