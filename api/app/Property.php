<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function propertyType()
    {
        return $this->belongsTo('App\PropertyType');
    }

    public function roomType()
    {
        return $this->belongsTo('App\RoomType');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function propertyAmenities()
    {
        return $this->hasMany('App\PropertyAmenity');
    }
}
