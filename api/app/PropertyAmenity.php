<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyAmenity extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function property()
    {
        return $this->belongsTo('App\Property');
    }

    public function amenity()
    {
        return $this->belongsTo('App\Amenity');
    }
}
