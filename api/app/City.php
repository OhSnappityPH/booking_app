<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $guarded=[];

    public function properties()
    {
        return $this->hasMany('App\Property');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }
}
