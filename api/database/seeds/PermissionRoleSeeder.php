<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        //Permissions related to Properties
        Permission::create(['name' => 'crud properties']);
        Permission::create(['name' => 'crud propertyTypes']);
        Permission::create(['name' => 'crud propertyReviews']);
        Permission::create(['name' => 'crud propertyImages']);

        //Permissions related to User Creation
        Permission::create(['name' => 'crud users']);

        //Permissions for Locations
        Permission::create(['name' => 'crud country']);
        Permission::create(['name' => 'crud states']);
        Permission::create(['name' => 'crud cities']);

        //Permissions for Categories
        Permission::create(['name' => 'crud categories']);
        Permission::create(['name' => 'crud subcategories']);

        //Permission for Neighborhoods and Types
        Permission::create(['name' => 'crud neighborTypes']);
        Permission::create(['name' => 'crud neighborhoods']);

        //Permission for Amenities
        Permission::create(['name' => 'crud amenities']);

        //Permissions for Bookings and Transactions and Promo Codes
        Permission::create(['name' => 'crud promoCodes']);
        Permission::create(['name' => 'crud bookings']);
        Permission::create(['name' => 'crud transactions']);

        //Roles Booking App 
        $role1 = Role::create(['name' => 'superAdmin']);
        $role1->syncPermissions(Permission::all());

        $role2 = Role::create(['name' => 'host']);
        $role2->givePermissionTo([
            'crud properties', 
            'crud users', 
            'crud transactions', 
            'crud bookings'
        ]);

        $role3 = Role::create(['name' => 'guest']);
        $role3->givePermissionTo([
            'crud transactions', 
            'crud bookings', 
            'crud users'
        ]);

        $role4 = Role::create(['name' => 'hostAndGuest']);
        $role4->givePermissionTo([
            'crud properties', 
            'crud users', 
            'crud transactions', 
            'crud bookings'
        ]);

    }
}
