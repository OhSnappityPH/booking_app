<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNeighborhoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('neighborhoods', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->unsignedBigInteger('neighbor_type_id');
            $table->string('latitude');
            $table->string('longitude');
            $table->text('tags');
            $table->unsignedBigInteger('added_by_user');
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::table('neighborhoods', function (Blueprint $table) {
            $table->foreign('neighbor_type_id')
                    ->references('id')->on('neighbor_types')
                    ->onDelete('cascade');
        });

        Schema::table('neighborhoods', function (Blueprint $table) {
            $table->foreign('added_by_user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('neighborhoods');
    }
}
