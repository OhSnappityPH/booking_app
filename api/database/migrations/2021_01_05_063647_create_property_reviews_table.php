<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_reviews', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('review_by_user');
            $table->unsignedBigInteger('booking_id');
            $table->text('comment');
            $table->string('rating');
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::table('property_reviews', function (Blueprint $table) {
            $table->foreign('property_id')
                    ->references('id')->on('properties')
                    ->onDelete('cascade');
        });

        Schema::table('property_reviews', function (Blueprint $table) {
            $table->foreign('review_by_user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_reviews');
    }
}
