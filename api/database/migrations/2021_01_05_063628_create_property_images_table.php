<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_images', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('added_by_user');
            $table->string('image');
            $table->SoftDeletes();
            $table->timestamps();
        });
        
        Schema::table('property_images', function (Blueprint $table) {
            $table->foreign('property_id')
                    ->references('id')->on('properties')
                    ->onDelete('cascade');
        });
        
        Schema::table('property_images', function (Blueprint $table) {
            $table->foreign('added_by_user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_images');
    }
}
