<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('receiver_id');
            $table->unsignedBigInteger('payee_id');
            $table->unsignedBigInteger('booking_id');
            $table->decimal('site_fees');
            $table->decimal('amount');
            $table->date('transfer_on');
            $table->unsignedBigInteger('promo_code_id')->nullable;
            $table->decimal('discount_amount')->nullable;
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('property_id')
                    ->references('id')->on('properties')
                    ->onDelete('cascade');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('receiver_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('payee_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('booking_id')
                    ->references('id')->on('bookings')
                    ->onDelete('cascade');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('promo_code_id')
                    ->references('id')->on('promo_codes')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
