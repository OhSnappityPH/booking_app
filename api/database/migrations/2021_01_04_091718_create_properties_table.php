<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('property_type_id');
            $table->unsignedBigInteger('room_type_id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('subcategory_id');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('state_id');
            $table->unsignedBigInteger('city_id');
            $table->text('address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('bedroom_count');
            $table->string('bed_count');
            $table->string('bathroom_count');
            $table->string('accommodates_count');
            $table->tinyInteger('availability_type');
            $table->date('start_date');
            $table->date('end_date');
            $table->decimal('price');
            $table->tinyInteger('price_type'); //{1: per night, 2: Week, 3: 2 wks, ETC}
            $table->string('minimum_stay');
            $table->tinyInteger('minimum_stay_type'); //{1: day, 2: week, 3: month, 4: year}
            $table->tinyInteger('refund_type'); //1 for cash, 2 for CC, 3 for idk
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });

        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('property_type_id')
                    ->references('id')->on('property_types')
                    ->onDelete('cascade');
        });
        
        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('room_type_id')
                    ->references('id')->on('room_types')
                    ->onDelete('cascade');
        });
        
        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('category_id')
                    ->references('id')->on('categories')
                    ->onDelete('cascade');
        });
        
        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('subcategory_id')
                    ->references('id')->on('subcategories')
                    ->onDelete('cascade');
        });
        
        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('country_id')
                    ->references('id')->on('countries')
                    ->onDelete('cascade');
        });
        
        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('state_id')
                    ->references('id')->on('states')
                    ->onDelete('cascade');
        });
        
        Schema::table('properties', function (Blueprint $table) {
            $table->foreign('city_id')
                    ->references('id')->on('cities')
                    ->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
