<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('user_id');
            $table->date('check_in_date');
            $table->date('check_out_date');
            $table->decimal('price_per_day');
            $table->decimal('price_per_stay');
            $table->decimal('tax_paid');
            $table->decimal('site_fees');
            $table->decimal('amount_paid');
            $table->boolean('is_refundable');
            $table->date('cancel_date')->nullable();
            $table->decimal('refund_paid')->nullable();
            $table->unsignedBigInteger('transaction_id');
            $table->decimal('effective_amount');
            $table->date('booking_date');
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->foreign('property_id')
                    ->references('id')->on('properties')
                    ->onDelete('cascade');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });

        // Schema::table('bookings', function (Blueprint $table) {
        //     $table->foreign('transaction_id')
        //             ->references('id')->on('transactions')
        //             ->onDelete('cascade');
        // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
