<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_amenities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('amenity_id');
            $table->SoftDeletes();
            $table->timestamps();
        });

        Schema::table('property_amenities', function (Blueprint $table) {
            $table->foreign('property_id')
                    ->references('id')->on('properties')
                    ->onDelete('cascade');
        });
        
        Schema::table('property_amenities', function (Blueprint $table) {
            $table->foreign('amenity_id')
                    ->references('id')->on('amenities')
                    ->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_amenities');
    }
}
