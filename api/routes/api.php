<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function ($router){
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('register', 'AuthController@register');
    Route::post('me', 'AuthController@me');
});

Route::apiResource('user', 'UserController');
// Route::apiResource('amenity', 'AmenityController');
// Route::apiResource('booking', 'BookingController');
// Route::apiResource('category', 'CategoryController');
// Route::apiResource('city', 'CityController');
// Route::apiResource('country', 'CountryController');
// Route::apiResource('neighborhood', 'NeighborhoodController');
// Route::apiResource('neighbortype', 'NeighborTypeController');
// Route::apiResource('promocode', 'PromoCodeController');
// Route::apiResource('propertyamenity', 'PropertyAmenityController');
// Route::apiResource('propertyimage', 'PropertyImageController');
// Route::apiResource('property', 'PropertyController');
// Route::apiResource('propertyreview', 'PropertyReviewController');
// Route::apiResource('propertytype', 'PropertyTypeController');
// Route::apiResource('roomtype', 'RoomTypeController');
// Route::apiResource('state', 'StateController');
// Route::apiResource('subcategory', 'SubcategoryController');
// Route::apiResource('transaction', 'TransactionController');